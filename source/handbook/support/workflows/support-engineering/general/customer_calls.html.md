---
layout: markdown_page
title: Customer calls
category: Handling tickets
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Customer calls

### Internally scheduled calls

All scheduled calls involving Customer Success, Sales, Development, and/or Support should be organized via the support project [issue tracker](https://gitlab.com/gitlab-com/support/support-team-meta/issues) using the [customer call template](https://gitlab.com/gitlab-com/support/support-team-meta/blob/master/.gitlab/issue_templates/Customer%20call.md).

The template can be used when coordinating a call with other engineering/sales team members or when scheduling another Support Engineer in a more preferable timezone. These calls may involve customers as well.

#### Intake, upgrade and installation support

For Premium Support customers, and customers who have purchased Implementation Support, we offer intake and installation support. Premium Support customers also receive live upgrade assistance. The different levels of service that are offered are described on the [support page](/support), and Implementation Support is described in more detail in the [support handbook](/handbook/support/#implementation-support).

Call/screen sharing sessions involve guiding customers through the GitLab upgrade process or taking control of the customers server to perform the upgrade. You should make sure that the customer has a backup before you start the call, as they can take a lot of time to complete and you don't want to do them while in the call. You should also make sure you know as

**Important information to collect**

1. Type of installation: Source/Omnibus
1. Current GitLab version
1. Version you're upgrading to (it isn't always the latest)
1. Use of GitLab CI (need to upgrade to 8.0 first, then 8.+)

We collect this information in Zendesk and link it to the organization, see the
[responding to tickets section in onboarding](/handbook/support/onboarding).

### Unscheduled calls

While engaging with customers you should always be prepared to jump on a call with them. It is easier to get
all the information you might need on a 20 minute call than on 10 2-minute emails. If a conversation goes through
several back and forth emails and the problem still isn't close to being resolved, suggest a call via Zoom.

If you feel too inexperienced to handle a call, ask someone more experienced to handle the call and
listen in if at all possible. After someone else had the call with the customer it is still your responsibility
to handle the ticket as long as the ticket is still assigned to you.

If the problem is urgent you can simply send the customer a Zoom link and jump on immediately. For less urgent
calls, you may send the customer a Calendly link so that they can schedule a convenient time in their time zone.
The link you send may be a link to your personal calendar or to the Team Calendly event, depending on the circumstances,
but in every case you should send a single-use Calendly link, to ensure proper processes are followed for scheduling
calls.

#### Generating a single-use Calendly link

Calendly has a [Chrome 
plug-in](https://chrome.google.com/webstore/detail/calendly-meeting-scheduli/cbhilkcodigmigfbnphipnnmamjfkipp) 
that makes it easy to generate a single-use link to send to the customer. After adding the plug-in to Chrome, look for
the Calendly icon at the top right in your browser. Sign in to Calendly, then you will see a list of events. You 
probably want to "star" the Team event and perhaps your personal Support Call event to simplify the pop-up window.
Then simply click on the one-time link icon next to the event of your choice to generate a link you can paste into 
your message to the customer.

![Calendly Chrome plug-in](/handbook/support/workflows/support-engineering/general/calendly.png)

If you do not want to use Chrome, you can generate a link to the Team Event using this curl command. Replace the
`<your Calendly API token>` placeholder with a token you can get from [the Calendly Integrations 
page](https://calendly.com/integrations).

```
curl -v  --header "X-TOKEN: <your Calendly API token>" --data "event_type_uuid=CEGFRWO2Q6R7SAQE" https://calendly.com/api/apps/extension/v1/users/me/event_type_single_use_links
```

#### Pre-call email

Please consider sending a pre-call email. This helps set expectations to the call regarding goals, duration, and
the people required to be on the call for effective troubleshooting. You can use the `General::Pre customer call` macro in Zendesk
for that, please modify it as you see fit.

#### On-call language to ensure call sticks to 1-hour

* Set expectations (again) at the start of the call

1. Call duration will be 1 hour
1. At 45 minutes - call wrap up will happen (below)
1. Will need access to applicable systems
* When 15 mins left: start to wind down the call
* Stop the call and determine where we are (solved, not solved more info needed)
1. Solvable in the next few minutes
1. Need to research/schedule additional call
* Review
1. Summary of what was learned
1. Next steps for GitLab agent
1. Next steps for user
1. Next call recommendations (timing/goals/expectations)

#### Call summary

Following your scheduled or unscheduled call you should complete a summary of the call in Zendesk using the
macro `General::Post Customer Call`.  This will provide a record of events for the next support agent in the hot queue
as well as the customer.  It will also provide valuable information for support agents in the future who search Zendesk
looking for similar issues and their resolutions.
