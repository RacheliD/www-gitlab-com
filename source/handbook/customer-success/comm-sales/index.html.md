---
layout: markdown_page
title: "Commercial Sales - Customer Success"
---

# Commercial Sales - Customer Success Handbook
{:.no_toc}

GitLab defines Commercial Sales as worldwide sales for the mid-market and small/medium business segments. [Sales segmentation](https://about.gitlab.com/handbook/business-ops/#segmentation) is defined by the total employee count of the global account. The Commercial Sales segment consists of two sales teams, Small Business (SMB) and Mid-Market (MM). The Commercial Sales segment is supported by a dedicated team of Solutions Architects (SA) and Technical Account Managers (TAM).

## On this page
{:.no_toc}

- TOC
{:toc}

## Role & Responsibilities

### Solutions Architects

Solutions Architects are aligned to the Commercial Sales Account Executives by a pooled model. Requests for an SA will be pulled from the triage board by an SA based on multiple factors including availability, applicable subject matter expertise, and workload.  

* [Solutions Architect role description](/job-families/sales/solutions-architect/)
* [Solutions Architect overview](/handbook/customer-success/solutions-architects/)
* [When and how to engage a Commercial Solutions Architect](/handbook/customer-success/solutions-architects/#commercial-engagement-model)
* [How incoming SA requests are triaged](/handbook/customer-success/solutions-architects/#triage-of-issues)

### Technical Account Managers

Technical Account Managers that support Commercial Sales are aligned by region, Americas East, Americas West, EMEA and APAC. Not all accounts will have a dedicated TAM. Account qualification is required.

* [Technical Account Manager role description](/job-families/sales/technical-account-manager/)
* [Technical Account Manager overview](/handbook/customer-success/tam/)
* [When and how a TAM is engaged](/handbook/customer-success/tam/engagement/)

## Seamless Customer Journey

A seamless customer journey requires a continuous flow of relevant information between the roles at GitLab with customer outcomes in focus. Below are some examples of the transfer of information between roles that may be required.

### TAM to TAM (existing accounts)

* Ensure all accounts have a GitLab project [here](https://gitlab.com/gitlab-com/account-management/commercial)
* Update TAM name on account team in Salesforce
* Share any Outreach sequences or templates currently in use
* The new TAM should be introduced live on a client call whenever possible
* Ensure any current action items are identified via an issue on the [Commercial TAM Triage board](https://gitlab.com/gitlab-com/account-management/commercial/triage/boards/1139879?&label_name[]=Status%3A%3ANew)

### Account Executive to TAM (existing accounts without a TAM)

* SA or TAM creates a GitLab project [here](https://gitlab.com/gitlab-com/account-management/commercial)
* Add TAM name to account team in Salesforce
* Identify any relevant Outreach sequences or templates
* The new TAM should be introduced live on a client call whenever possible
* Ensure any current action items are identified via an issue on the [Commercial TAM Triage board](https://gitlab.com/gitlab-com/account-management/commercial/triage/boards/1139879?&label_name[]=Status%3A%3ANew)

### SA to TAM (new accounts)

* SA creates a GitLab project [here](https://gitlab.com/gitlab-com/account-management/commercial)
* Ensure any account notes held outside the project are linked to Salesforce and shared with the TAM
* SA to clearly outline to TAM how far the customer is in their adoption of GitLab
* TAM begins Outreach sequence for new customers
* Add TAM name to account team in Salesforce, ensure SA name is already present
* Ensure any urgent action items are identified via an issue on the [Commercial TAM Triage board](https://gitlab.com/gitlab-com/account-management/commercial/triage/boards/1139879?&label_name[]=Status%3A%3ANew)

### SA to SA (new accounts)

* Update SA name on account team in Salesforce
* Ensure any account notes are linked to Salesforce and shared with the new SA
* Introduce new SA live on a client call
* If a [POC](/handbook/sales/POC/) is pending or active, update the POC record in Salesforce as required
* Ensure any current action items are identified via an issue on the [Commercial SA Triage board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/commercial-triage/boards/1006966) 

## Customer Engagement Guidelines

### Ongoing Customer Communication

For commercial accounts, we do not currently offer our customers Slack channels unless they meet specific criteria and it would be highly beneficial to both the customer and GitLab teams. Prior to creating the channel and provisioning access, the TAM for the account must approve its creation and use. To qualify, the customer should be at minimum:
- Premium/Silver or above
- 200k+ IACV

[Collaborative projects](/handbook/customer-success/tam/engagement/#managing-the-customer-engagement) should be suggested first with few possible exceptions (such as reference customers, early adopters or strategic partnerships).  If the customer is evaluating GitLab or doing a POC for an upgrade, SAs or TAMs can request an ephemeral Slack channel, but the channel should be closed within 90 days if it does not meet the criteria shown above. Slack is simply too intensive to scale for Mid-Market and SMB.

Meanwhile, it is not always scalable for every customer to have a collaboration project either, and the CS team will determine the need for a project on a per-customer basis.

### SMB Presales Customer Engagement

Since the SMB team does not currently have Solutions Architects, the SMB team may engage the Mid-Market SA team for presales support using the following guidelines:
  
#### Asynchronous Presales Support

SMB Customer Advocates may request asynchronous technical presales assistance for any SMB prospect using the Commercial Triage Board via [the process](/handbook/customer-success/solutions-architects/#commercial-engagement-model) documented on the Solutions Architects page. The Customer Advocate should obtain as much detail as possible and post the technical questions from the client into an issue on the Commercial Triage board.  

The Mid-Market SA team will respond in the issue with answers within 48 hours unless otherwise noted by the SA team. 

#### Synchronous Presales Support

SMB Customer Advocates may request synchronous technical presales assistance when opportunity threshold criteria are met. This request for real-time presales assistance should utlize the Commercial Triage Board via [the process](/handbook/customer-success/solutions-architects/#commercial-engagement-model) documented on the Solutions Architects page. If the prospect does not meet the criteria below, Customer Advocates may still inquire about surplus bandwidth and/or availability of the Mid-Market SA team using the #cs-commercial channel in Slack.

Criteria for Synchronous Engagement:  
  * Minimum engagement threshold is $9K IACV
  * Preference will be given to Ultimate/Gold or Premium/Silver prospects when bandwidth is limited

Engagement Limitations: 
  * No more than 2 synchronous presales engagements (60 minutes total) may occur per account
  * No guided Proof of Concept or dedicated trial guidance is available

