---
layout: markdown_page
title: "GitLab 101"
---

## GitLab 101s

### CEO 101

There is a monthly GitLab CEO 101 call with new hires and the CEO. Here we talk about the following topics. This is a zoom call that will be a public stream to YouTube. Please read the history and values links provided below and come prepared with questions added to the notes doc included in the calendar invite. The call will be driven by questions asked by new team-members. Make sure your full name is set in Zoom with your title.

1. Questions about GitLab history [/company/history/](/company/history/)
1. Questions about our values [/handbook/values](/handbook/values)
1. Team structure [/company/team/structure/](/company/team/structure/) or [organization chart](/company/team/org-chart)
1. How we work [/handbook/general-guidelines](/handbook/general-guidelines)
  - Please pay special attention to the guideline about developing content and procedures in a public, transparent, [handbook-first](/handbook/general-guidelines/#handbook-first) manner

Note that, when thinking about what questions you'd like to ask, it’s hard to be open about financials. It’s easy when everything is going well but very hard when it’s not. We try to be open with our team as we progress, but it will hurt being open to the outside world about this in the future.

All questions and answers during the call should be added to the this page or to the relevant pages of [the handbook](/handbook/). This is done during the call to immediately make any changes; for example we created [this commit](https://gitlab.com/gitlab-com/www-gitlab-com/commit/8cf1b0117dce5439f61e207315f75db96c917056) together on the call.

The calendar invite will include an agenda google doc where those participating in the meeting can add their questions and a link to this handbook page. If you have any questions please contact the CEO Executive Assistant in #ea-team on slack.

Below a few examples of the GitLab 101 calls

<figure class="video_container">
  <iframe width="560" height="315" src="https://youtube.com/embed/YovTmwsMrQo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/FALTpdV6dsw" frameborder="0" allowfullscreen></iframe>
</figure>


### New Teammates Introduction

Immediately before the CEO 101, there will be a separate meeting for new teammate introductions. This gives everyone on the call a chance to get to know each other before the Q&A starts, and saves as much time as possible for the CEO to take questions. Each introduction should take less than 60 seconds. It is surprising how much we can communicate in a minute and we want to ensure that everyone has a chance to introduce themselves. A member from the PeopleOps Team will be in attendance to ensure this happens on time and is recorded. Please introduce yourself using the order in the calendar invite and share:

  - What do you do at GitLab?
  - Why did you join GitLab?
  - What do you enjoy in your private life (quirky details are encouraged, also feel free to introduce your significant other and/or pets)?
  - Did you already have 5 [virtual coffee chats](/company/culture/all-remote/tips/)?
  - If you already had 5 are you open to more?

Please keep your introduction to less than 60 seconds. After you're finished with your introduction, invite the next person to talk.



### Frequently Asked Questions about the GitLab Culture

##### All previously asked questions have been integrated into our [handbook](/handbook/)

Future questions will be added to this FAQ section.
