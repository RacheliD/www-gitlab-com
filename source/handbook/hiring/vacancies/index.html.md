---
layout: markdown_page
title: "Vacancies"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

For a listing of open vacancies, please see the [vacancies section on the Jobs page](/jobs/apply). Vacancies are maintained in Greenhouse based on our [job families](/handbook/hiring/job-families). A vacancy is a temporarily open position. A job family is a permanent description that also applies to existing people with that title. Don't use vacancy and job family interchangeably. The relevant parts of a job family are copy-pasted to Greenhouse to open a vacancy.

#### Vacancy Creation Process

To create a vacancy, a Hiring Manager will work with their Recruiting Manager, as outlined in the steps below. To note, a vacancy can only be created by the Department's Recruiting Manager before it can be submitted for approval in Greenhouse. This process enables us to ensure Greenhouse the single source of truth for hiring metrics.

#### Adding Vacancies to the Hiring Plan

The hiring manager carries the responsibility to ensure vacancies are opened in Greenhouse. Opened vacancies should align to what is included in the rolling 4Q forecast. Therefore, the Hiring Manager, Finance, and Recruiting can stay in sync on what was planned and track towards those goals.


#### Create or Review the Job Family

Once the vacancy is approved, the Hiring Manager will review the position description in the `/job-families` folder and update it, if needed, by making a merge request and assigning it to the Executive to review and merge. If there is no existing job family, follow the steps to [create a new job family](/handbook/hiring/job-families/#new-job-family-creation), then continue. Please note that new job families require approval from the CEO. The Recruiting Manager will use the relevant elements from this job family to open the vacancy in Greenhouse. The job family should include:

  - Responsibilities
  - Specialties (e.g. Gitaly, Backend)
  - Levels (e.g. intern, junior, senior, staff, manager)
  - Location (e.g. Anywhere, EMEA, Americas)
  - Requirements
  - Hiring Process, also update the internal [hiring process repo](https://gitlab.com/gitlab-com/people-ops/hiring-processes)

A compensation benchmark for the role should be set by working with the People Operations Analyst.

#### Opening Vacancies in Greenhouse

For positions that are included in the Rolling 4 Quarter Forecast, the Recruiting Manager will create and open all approved vacancies simultaneously.

The Recruiting Manager will [log in](https://about.gitlab.com/handbook/hiring/greenhouse/#how-to-join-greenhouse) to [Greenhouse](https://app2.greenhouse.io/users/sign_in) and hover over the plus sign in the top right corner of their dashboard, and then click ["Create a Job"](https://app2.greenhouse.io/plans/new).

Click on ["Start from a copy of an existing job"](https://app2.greenhouse.io/get_started/show_existing_jobs). From there, you can choose to clone a vacancy, which is ideal if you are opening a vacancy that has been opened before, or something very similar has been opened before. If there is no close match, select `Any Status` at the top bar, select "Draft", and choose the template for the division. Please note: when creating a vacancy, the "Start from scratch" option should not be used, as important elements needed in a vacancy will not be included.

- Once you've selected a vacancy to copy, the first screen will ask for **basic job info**.
  - The "Internal job name" is only viewable within Greenhouse, and the "External job name" is what appears on the jobs page. Enter in the vacancy name in these fields, including the speciality if applicable. These two fields should almost always be the same, but if you have questions on this, please reach out to the recruiting team.
  - "Department" is the department this role will fall under. Always choose a department and **not** a division (e.g. for a Security Engineer vacancy, choose the "Security" department underneath the "Engineering" division, and do not choose "Engineering"). If you are unsure of what department to choose, reach out to the recruiting team.
  - If a vacancy can be located anywhere where GitLab hires, check "Anywhere" next to "Office". If a vacancy is dedicated to a certain region or timezone, uncheck the "Anywhere" button and select any of the predetermined regions or timezones. If you need a region or timezone not represented in the  list, please reach out to the recruiting team. In addition to the region or timezone you've selected, it's recommended to also choose "Remote" so that it's clear to applicants that the vacancy is both located in a particular area but also still remote.
  - Please input the number of openings you want to hire for this role.
  - Click `Generate IDs` to create a requisition ID and opening IDs for your vacancies, which the recruiting team will use to help keep track of them.
  - Under "Employment Type", select if your vacancy is a full-time, part-time, intern, or contract role. Please note: "contract" here refers to true independent contractors who work on short-term projects and is rarely used.
  - "Salary"
    - For roles in the compensation calculator: Take the `benchmark x level x 0.25` for the low end and `benchmark x level x 0.8` on the high end.
    - For roles not in the compensation calculator (Sales/Director/Exec): Leave blank if not known. The People Operations Analyst will edit as the first level of approval. People Ops Analysts will pull survey data in San Francisco for this role then apply the same formula for `0.25` on the low end and `0.8` on the high end. 
  - If this vacancy is eligible for a bonus, input the range of the bonus amount under the "Bonus" field. If there is no bonuses associated with this vacancy, leave it blank.
  - If this vacancy is eligible for stock options, input the [range of offered stock options](https://about.gitlab.com/handbook/stock-options/) under the "Options" field. If you may be hiring various levels for this vacancy, input the range (e.g. if you may hire anywhere from Junior to Senior in put a range), but if you only want to hire Seniors, then input the number of stock options associated with that level in both fields. If there are no stock options associated with this vacancy, enter 0.
  - Under "Type" choose if this is a new hire or a backfill.
  - Select if you'd like to be signed up for weekly recruiting emails or new candidate emails. You'll also be able to configure your notifications later, so don't worry if you're not sure yet.
  - On the righthand side, you'll be able to add optional background information for interviewers and advice on how to sell this vacancy. This is not required but gives great context for the interviewers and is recommended.
  - Click "Create Job & Continue".
* The next page consists of all of the **attributes** interviewers will be evaluating for candidates in their scorecards across the full interview process.
  - The attributes are typically split up into various categories, such "Skills", "Qualifications" "Personality Traits", "Details", and "Values-Alignment". These can be adjusted as needed, but **every** attribute listed should be a must-have and not a nice-to-have. If you want to include nice-to-have's in the scorecard, please create a new category called "Nice-to-have's" and add any applicable attributes there, making sure that your entire interview team knows that if a candidate does not meet any of those attributes it is not a negative against them.
  - To create a new category, scroll to the bottom of the screen and select "Add a Category", and add the name of the category. Try to have as few categories as possible and lump as many attributes under each category as possible.
  - To remove a category, hover over the category, and click "Delete Category". The only required category is "Values-Alignment", which should never be deleted, but other relevant categories such as "Skills" typically should be kept as well.
  - To edit the attributes within a category, click "Edit" next to the category. You can then change the name of attributes, delete attributes, add brand new ones, or choose existing attributes from other vacancies. Keeping the attribute names the same and choosing existing attributes from other vacancies is recommended, so that the attribute choices remain streamlined. However, if you have a particular attribute for this role that needs to be evaluated, don't hesitate to add it. Click "Save" once you've configured your attributes under the category, do the same for each of the other categories. If you don't want to save your changes after you've clicked "Edit" under a category, click "Cancel". Once the scorecard is finished, click "This looks good, NEXT" at the right of your screen.
* The next section is the **interview plan**, where you'll craft the hiring process and scorecards for each step in the process.
  - Every vacancy should have an "Application Review" stage. Typically no edits are needed on this stage.
  - Many vacancies have an assessment as the first step in the process.
    - If your vacancy requires an assessment but there is no assessment stage already added, scroll to the bottom of the page and click "Add a Stage".
      - From there, you can either click "Copy from another job?" at the bottom of the pop-up and select a vacancy you know has an assessment. Click on "Assessment", then "Add."
      - If you're not sure of another vacancy that has an assessment, you can scroll to the bottom of the pre-populated list and select "Take Home Test", then "Add". Once you've added it, hover over the new stage and click the pencil next to the stage name and change the text from `Take Home Test` to `Assessment`. Then hover over the second "Take Home Text" on the right of the stage and change the text again.
      - The assessment stage **must** be added using one of the two methods above, or it will not work properly. If you have any questions about this, please reach out to the recruiting team.
    - Once your assessment stage is created, or if it is already included, click "Edit" in the stage. You'll then want to select any attributes you want the grader of the assessment to focus on. This will typically be more technical in nature, but select whatever seems appropriate.
    - Scroll down to "Email to Candidate". This is where you'll include the actual assessment questions. The "From" should be `{{MY_EMAIL_ADDRESS}}` and "Subject" `GitLab Application - {{CANDIDATE_NAME}} - {{JOB_NAME}} Questionnaire`. In the Body, craft an email and insert your assessment questions. Below the body, make sure that the "link for candidates to submit tests" is **ON**. You can also add any attachments below that field if necessary.
    - Scroll down to "Grading Instructions" and include any specific items you want your graders to look out for when they review the candidates' answers. You can copy this section over from another job if applicable.
    - Under "Custom Questions", be sure that there is either a "Full Notes" custom question or that you add one. To add it, simply click "Add Custom Question", title it "Full Notes", choose "Text" as the answer type, and click "Add Custom Question". If there are any other specific questions you want your graders to answer when reviewing the assessment, feel free to add them here. They can be required or not, depending on your preference.
    - Under "Graders and Notifications", search for members of your team who can grade the assessments. You can select multiple people at this point, and when the assessments are actually sent out to candidates, each grader will appear and the person sending the assessment can delete extras so it is only sent to one person. You can also select who you want to be notified when the test is received; the test graders should absolutely be selected, and it's recommended for both the recruiter and coordinator to be notified as well. You can select any additional people to be notified as well if desired.
    - Finally, under "Additional Settings", check "This interview requires scorecards to be submitted" and leave unchecked "Hide candidate name and details from grader."
    - Then click "Save"!
  - The next stage is the screening call stage, which should be standard across the board. It is recommended to click "Edit" on this stage, scroll to the bottom, and choose your recruiter as the default interviewer. This stage should already be otherwise configured.
  - The next stage is usually the team interview, where the candidates will meet with peers and the hiring manager.
    - Under this stage, you should see multiple interviews. They are typically called "Manager Interview", "Peer Interview 1", "Peer Interview 2", etc. You can also add additional interviews such as "Director Interview", "Demo with Panel", "Behavioral Interview", "Technical Interview", or other names that work for your hiring process.
    - To add a new interview in a stage, hover over the stage and click "Add Interview", then you can copy over an interview from another job or create a new one. You can also move the interviews around within the stage and change the interview names (though consistency is highly desired!).
    - For each interview in the stage, click "Edit" next to it. First, select the appropriate attributes to focus on in that interview. Then to the right of "Interview Prep", choose how long the interview should take (e.g. 30 minutes, 45 minutes, 60 minutes, etc.). Then include the purpose of the call and sample questions the interviewer should ask. You can copy this over from another vacancy if applicable.
    - Under "Custom Questions", be sure that there is either a "Full Notes" custom question or that you add one. To add it, simply click "Add Custom Question", title it "Full Notes", choose "Text" as the answer type, and click "Add Custom Question". If there are any other specific questions you want your graders to answer when performing this interview, feel free to add them here. They can be required or not, depending on your preference.
    - You can then choose default interviewers. If you have multiple team members that can interview, feel free to input all of their names, and the coordinator will choose one interviewer when scheduling the interview. This gives visibility into everyone who is trained and ready to perform interviews for this vacancy.
    - The two "Additional Settings" should both be checked.
    - Then click "Save"!
  - The next stage is the executive interview stage, where there are two interviews with the executive for the division of the vacancy as well as the optional CEO interview. The executive interview can be customized as needed following the guidelines for the team interview, including selecting attributes, adding custom questions, and selecting a default interviewer. The optional CEO interview does not need any customization besides adding a "Full Notes" custom question.
  - The next stage is for reference checks, with two sections for a former manager and a former peer of the candidate. These can be customized as needed.
  - The last stage is the offer stage and cannot be edited or removed.
  - Some important notes about the interview plan:
    - Every interview should be evaluating values-alignment in the attributes section.
    - On rare occasion, there may be additional or less stages than represented here, but these stages should be consistent as much as possible in order to maintain data integrity for reporting. The interviews within the stages can be adjusted as needed, as long as they follow the same names (e.g. there should only be one `Peer Interview 1` across all jobs and not a `Peer Interview 1` on one job and a `Peer Interview One` on another). If there is any doubt or confusion, feel free to reach out to the Talent Operations Specialist.
    - Any interview can be skipped depending on the candidate.
    - If a candidate will have more interviews in a stage than predetermined, you can add additional interview events as long as the candidate is in the stage where you need to add the additional event.
  - Once your interview plan is complete, click "This looks good, NEXT" at the right.
* The next section is the **hiring team**, where you'll select who will be working on this vacancy and what access they should have.
  - The first step is to scroll down to the "Who Can See This Job" to assign permissions to the team members who will need access. Continue scrolling to "Job Admin: Hiring Manager" and click the pencil and add the Hiring Manager(s), including their Managers, Directors, and Executive, then click save. Be sure to only include the Hiring Manager and above, as this will give them advanced permissions to view confidential information. Continue scrolling to the bottom of the page and under "Interviewers" select every person who will be in the hiring process, whether they will be grading assessment, reviewing new applications, or performing interviews.
  - Scroll back to the top of the page and select the main people responsible for the job. Under "Hiring Managers", click the pencil and select the Hiring Manager for the vacancy and click save. Under "Recruiters", select the [recruiter assigned to your division](https://about.gitlab.com/handbook/hiring/recruiting-alignment/). Under "Coordinators", select the [coordinators assigned to your division](https://about.gitlab.com/handbook/hiring/recruiting-alignment/). Assigning these roles is highly important so that the team receives the appropriate notifications and access. You can also select the Sourcer assigned to your division/region, but it is not necessary as there are no specific Sourcer tasks.
  - Once the hiring team is added, click "This looks good, NEXT" at the right.
* The next section is the **approvals** section. You can add any notes here for the vacancy where it says "Leave a note". At this point, scroll to the bottom of the page and click "Request Approval". You'll be redirected to the vacancy in Greenhouse, where you can review the vacancy, make any changes, or make any updates. Once approved by [all approvers](https://about.gitlab.com/handbook/hiring/greenhouse/#approval-flows), the vacancy will open and the recruiting team will begin the process! Your recruiter will be in touch within 48 hours of the vacancy being approved to hold an intake session. You can return to the draft vacancy in Greenhouse at any time to check the status, or feel free to reach out to the recruiting team with any questions.
* **Double check that your vacancy posted correctly.** There's been at least one case where a vacancy was not sent to the approvers after the Hiring Manager requested approval. To confirm that your vacancy was created correctly:
  - Click the [All Jobs](https://app2.greenhouse.io/alljobs) link at the top of the page in Greenhouse.
  - Click the **Filter** dropdown and change "Job Status" to "Draft" and be sure no other filter options are selected.
  - You should see your new vacancy listed there. If it was submitted correctly, the job title should be followed by "(Approval Pending)." If it isn't, then you may need to click back into the position and click "Request Approval" again.
  - If you have any issues or questions, please reach out to the recruiting team.

#### Recruiting Team Tasks

After a Recruiting Manager creates a vacancy in Greenhouse and requests approval, it goes through the approval chain. The final approver of a vacancy is a member of the recruiting team, typically the [Candidate Experience Specialist](https://about.gitlab.com/job-families/people-ops/candidate-experience-specialist/) who is the coordinator for that vacancy. They should follow the below steps once they receive the request to approve a new vacancy.

* Go to the vacancy and under "Job Setup", review the configurations the Recruiting Manager has made and ensure everything is correct; reach out to them to ask clarifying questions if there is anything you don't understand or there is anything missing.
  - Pay special attention to the interview plan, ensuring any assessments were correctly added and that values alignment is a suggested attribute for every interview.
* Confirm each person in the hiring process has appropriate access and upgrade if necessary for both [Greenhouse](https://about.gitlab.com/handbook/hiring/greenhouse/#how-to-upgrade-access-levels) and [Zoom](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#make-zoom-pro) (being sure to provision them a personal Zoom link).
  - You can upgrade their Greenhouse access from within the vacancy by scrolling to the bottom of the "Hiring Team" section of the vacancy under "Job Setup" and adding their name to appropriate section. Hiring Managers can only be assigned to the vacancy as the "Hiring Manager" after they are added to the "Job Admin: Hiring Manager" (or "Job Admin: Job Approval" if the hiring manager is an executive) access group.
* Set up email **notifications** for the hiring team under "Job Setup". There are some recommended notifications below. [Notifications can be set up]((https://support.greenhouse.io/hc/en-us/articles/201341044-Who-can-set-up-email-notifications-) by and for any Job Admin for the vacancy, so hiring managers, recruiters, and coordinators can all customize their own notifications if they prefer something different.
  - Weekly Recruiting Report (for the Hiring Manager)
  - New Internal Applicants (for the Hiring Manager and Recruiter)
  - New Referrals (for the Hiring Manager and Recruiter)
  - New Agency Submissions (for the Hiring Manager and Recruiter)
  - Approved to Start Recruiting (for the Hiring Manager, Executive, Sourcing Manager, Candidate Experience Specialist, Candidate Experience Manager, Recruiter, Recruiting Managers)
  - Offer Fully Approved (for the Hiring Manager, Executive, Recruiter, Recruiting Manager(s), Sourcer, Sourcing Manager, Candidate Experience Manager and Candidate Experience Specialist)
  - Stage Transitions (for the Recruiter and Candidate Experience Specialist)
  - Additionally, add the Hiring Manager to the following Stages:
     -  Reference and Background Check 
     -  Justification 
     -  Offer 
  - New Scorecards (for the Recruiter and Candidate Experience Specialist)
* You will need to update the **job post** under "Job Setup", which will hold the vacancy description and application questions. Next to the name of the vacancy, click the pencil icon to edit the job post.
  - `Job Name` should not need to be edited, as the Hiring Manager included this when creating the vacancy.
  - `Post To` should always be `GitLab`.
  - `Location` should carry over from when the Hiring Manager created the vacancy, but feel free to edit as needed, always keeping "Remote" included.
  - `Application Language` should always be `English`.
  - `Description` should be the copy/pasted from the job family on GitLab.com with the relevant information for the level/speciality/etc.
    - If there are any links in the description, click on the link, then click the link icon in the text box toolbar, then change `Target` to `New Window`, then click "Ok" and repeat for any other links in the description; this will ensure all links work properly. To make sure this role is added to LinkedIn, you'll need to add a job wrapping code to the description.
    - After pasting in the relevant information from GitLab.com, click on the `< >` on the menu bar in the description text box to open the source code, scroll to the bottom of the pop up, copy and paste the following text `<div><span style="font-size: xx-small;"><span style="color: white;">Remote-ATL</span></span></div>` at the bottom, and change the letters in `Remote-XXX` to the appropriate code for the current [available LinkedIn job wrapping locations](https://docs.google.com/spreadsheets/d/1JrKLyMlrNgUpNEvEolM1YwvUGY7qm-J_CmJhW8sIDDE/edit#gid=186847216) (ensure to copy exactly from the sheet as spaces matter). Then click "Ok".
  - For the `Basic Application Questions` section, make sure "Location" is **required**, and "Phone", "Resume", "Cover letter" are all optional. Almost always we want to hide "Education", but discuss this with the Hiring Manager/Recruiter; if you do want to include it, make sure it is **optional** and not required.
  - For the `Additional Questions` section, double check that the questions were copied over from another job (an easy way to check this is to see if there are any GitLab-specific questions). If not, be sure to delete any existing questions, then copy the questions over from another job you know is correct so that every application is consistent and our reporting is accurate. The following questions should be included (if you do not see these, try copying from a different job and then going back to fix the first one you copied from):
    - Preferred  Name - **Optional**
    - LinkedIn Profile - **Optional**
    - GitLab Username - **Optional**
    - Please choose the country in which you are located. - **Required**
    - Are you legally eligible to work for GitLab where you are currently located? - **Required**
    - Were you referred by a current GitLab team-member? If so, please write their name below. - **Optional**
    - What are your pronouns for others to use when referring to you? (ex: she/her, he/him, they/them, ze/zir/hir, etc.) - **Optional**
    
  - You can add additional custom application questions as needed to help sort through required qualifications. 
  - Under `Settings`, make sure the question "Should EEOC questions be included in the application form?" is marked as "**Yes**" unless the role is internal and/or outside of the US.
  - Under `Settings`, make sure the question "Should applicants receive an automatic email when they submit an application?" is marked as "**Yes**" and that you choose the "_Default Candidate Auto Reply_" email template.
  - The final step is to choose a city to index the vacancy on the free LinkedIn Limited listing job board. Please reference where [GitLab cannot hire](https://about.gitlab.com/jobs/faq/#country-hiring-guidelines) in order to avoid confusion. This is a required step.
  - You can click the "Preview" button at the bottom of the page to preview what the vacancy posting will look like on Greenhouse.
  - When you're ready, click "Save" at the bottom of the screen.
* Go back to the `Approvals` tab and approve the job in Greenhouse.
* Then go to "Job Setup" > "Job Posts", and click the red button to publish the vacancy to our external jobs page.
* Next, we'll publish the vacancy to our internal jobs page.
  - Click the three dots next to the pencil and click "Duplicate".
  - The `Job Name` automatically put "Copy of" at the front of the title; remove this.
  - Change `Post To` to `INTERNAL`.
  - Keep `Location`, `Application Language`, `Description`, and `Basic Application Questions` the same.
  - The `Additional Questions` section should be altered to only have the following questions:
    - LinkedIn Profile - **Optional**
    - What are your pronouns for others to use when referring to you? (ex: she/her, he/him, they/them, ze/zir/hir, etc.) - **Optional**
    - GitLab Username - **Optional**
  - Under `Settings`, "**No**" for the EEOC questions and automatic email should be marked as "**Yes**", change the email template to "_Internal Auto-Reply after Application_".
  - Click "Save" at the bottom of the screen.
  - Click the red button to publish the vacancy to our internal jobs page.
* Create a merge request to add the vacancy to the team page.
* After merging, announce on Slack in `#new-vacancies` for team members to apply or send in referrals. An example message is
* New Job Alert: TITLE
   Apply internally: LINK
   You can share this opening with your network: LINK
   Review how to make a referral: https://about.gitlab.com/handbook/hiring/greenhouse/#making-a-referral

Please note, updates from the [Greenhouse jobs page](https://boards.greenhouse.io/gitlab) to the [GitLab jobs page](https://about.gitlab.com/jobs/apply/) generally happen within 30 minutes. Though, it can take up to 1 business day.

## Sourcing, Screening, and Resume Submittal

1. When a vacancy is opened a Recruiter conducts an intake call with the Hiring Manager to get details about the position.
1. The vacancy is posted in Greenhouse and on relevant internet job boards.
1. The Recruiter may conduct a calibration exercise with the Hiring Manager by presenting 2-3 unscreened resumes to make sure they are identifying the right skills and will adjust accordingly.
1. The Recruiter conducts direct sourcing efforts, reviews resumes, maintains the status of applicants, sets dispositions, and will screen on average 8-10 candidates.
1. Once candidates have been identified and screened, the Recruiter will submit the top 4-5 qualified applicants to the Hiring Manager or designee for review.
1. the Manager will select which candidates they are interested in interviewing.

### Sourcing for Open Positions

For difficult or hard-to-fill positions, the hiring team will use available tools to source for additional candidates. Please communicate with the hiring team if sourcing is needed for a strategic, specialized, or difficult to fill position. In addition, Managers should also reach out to their own network for candidates and referrals. It is common for candidates to respond more frequently to those who they know are the hiring manager. One superpower of great Managers is having a strong network of talent from which to source.

Recruiters dedicate Fridays as "sourcing day". They use various tools to identify talent by proactively reaching out to candidates for opportunities. In addition, they will partner with hiring managers to participate in sourcing to ensure they are targeting the right skillsets.

## Publicizing the Vacancy

The Manager should always ask the team for passive referrals for open positions. GitLab team-members can refer candidates through our [referral program](/handbook/incentives/#referral-bonuses).

The hiring team will **always** publicize the vacancy through the following means:

1. Tweet the new vacancy post with the help of the content marketing manager and team.
1. Request "soft” referrals by encouraging all GitLab team-members to post links to the jobs site on their LinkedIn profiles.
1. [Who's Hiring](https://news.ycombinator.com/ask): On the first of the month (or closest business day), include a comment for GitLab in the Hacker News thread of "Who's Hiring". Template text:
`GitLab | Remote only | Full time - It’s an exciting time to join GitLab. As a fast-growing, all-remote company, GitLab is a place where you can contribute and make an impact from anywhere in the world. You’ll be part of an ambitious, productive team that values transparency and collaboration. We’re hiring solutions architects, engineering managers, security engineers, recruiters, support engineers, product managers, strategic account leaders, and more roles in every department. Here’s our full list of vacancies: https://about.gitlab.com/jobs/ Here’s an inside look at GitLab’s interview process from one of our new team members: https://about.gitlab.com/2019/03/28/what-its-like-to-interview-at-gitlab/ Want to learn more? Check out our company handbook: https://about.gitlab.com/handbook/`[Example comment](https://news.ycombinator.com/item?id=19543942)

**Note**: The hiring team may advertise the vacancy through the following sites and is open to posting to more, in order to widely publish a variety of vacancies:

1. [Alumni post](https://news.ycombinator.com/jobs) as a Y Combinator alumni we can post directly to the front page of Hacker News. We do this for all engineering functions, including the management ones. The PeopleOps vault has credentials under Hacker news. Go to [Alumni post](https://news.ycombinator.com/jobs) and click submit job. For the title use: `GitLab (YC W15, All-remote) is hiring XXX`. And the URL links to the vacancy. [Example posting](https://news.ycombinator.com/item?id=19586072)
1. [LinkedIn](https://www.linkedin.com/) (Able to post 40 vacancies simultaneously, please mention to hiring team if you want your role listed here and it is not already)
1. [RemoteBase](https://remotebase.io/) (Free; position descriptions are synced directly to our respective position description sites)
1. [WeWorkRemotely](https://weworkremotely.com) ($200 for 30 days, per position, used infrequently)
1. [RemoteOK](https://remoteok.io) ($200 for 90 days, per position, used infrequently)
1. [Indeed Prime](http://www.indeed.com/) (Primarily used for non-engineering roles)
1. [Ruby Weekly](https://rubyweekly.com) ($199 per slot per newsletter, for engineering roles)

When using vacancy links to spread the word about our current vacancies, in order to keep data accurate, we can create specific tracking links through Greenhouse in order to include the specific source of different job boards, etc. To learn more about how to create the tracking links for jobs, please [see the Greenhouse help article](https://support.greenhouse.io/hc/en-us/articles/201823760-Create-a-Tracking-Link-for-Your-Job-Board).

All vacancies must be posted on our jobs page for at least 3 business days before we can close it or make an offer; this includes all new positions and [promotions](/handbook/people-operations/promotions-transfers/#promotions). If a vacancy has been opened for at least 3 business days and has 50 or more applicants, the recruiting team will close the role to new applicants at that time and reopen only if and when we need more applicants.

## Updating a Vacancy

If any details of the vacancy need to be updated after it has been opened, the Hiring Manager can edit the details by clicking on "Job Setup,"" then "Edit" in the "Job Details" section. Some fields, such as the "No. of Openings," are restricted and will require additional approvals. In that scenario, please contact the Recruiting Manager or Greenhouse Administrator for assistance.

When the rolling 4 quarter forecast is approved the Recruiting Manager will increase the position count for the vacancy or create a new vacancy altogether.  

## Closing a Vacancy

To close a vacancy:

1. The hiring team will clear the pipeline of candidates in all stages of application and notify the candidates that the position has been either filled or closed. Consider rejecting promising candidates with the reason `Future Interest` and making them a prospect so we can reconsider them in the future. You can also add various tags to the candidates, which makes it easier to find them in Greenhouse later on if you are recruiting for the same or a similar position. You can also set a reminder for a candidate if you anticipate reopening the role at a later date.
1. Ask a Greenhouse admin (ideally your Recruiter or Candidate Experience Specialist) to close the position in Greenhouse.
1. At times, we may still be interviewing but also have a sufficient supply of candidates and need not accept new applicants to fill the vacancy. In this case, the hiring team will "drain" the vacancy. This means that the vacancy will be closed, but the existing pipeline will continue through the process.

If the position was posted on any external job sites, the hiring team will email the partner or remove the position from that site.
