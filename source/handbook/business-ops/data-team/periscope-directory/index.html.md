---
layout: markdown_page
title: "Periscope Directory"
description: "GitLab Periscope Directory"
---

## On this page
{:.no_toc}

- TOC
{:toc .toc-list-icons}

{::options parse_block_html="true" /}

----

Request Access to Periscope with an [Access Request Issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New+Access+Request)

## Periscope Resources
* [Periscope Training](https://drive.google.com/file/d/1FS5llpZlfvlFyYL-4kpP3YUgI98c_rKB/view?usp=sharing) (GitLab Internal)
* [Periscope Editor Training](https://drive.google.com/file/d/15tm_zomS2Ny6NdWiUNJlZ0_73THDiDww/view?usp=sharing) (GitLab Internal)
* **[Periscope Data Onboarding: Creating and Analyzing Charts/Dashboards](https://www.youtube.com/watch?v=F4FwRcKb95w&feature=youtu.be)**
* **[Getting Started With Periscope Data!](https://doc.periscopedata.com/article/getting-started)**
* [Periscope Guides](https://www.periscopedata.com/resources#guides)
* [Periscope Community](https://community.periscopedata.com)
* [Documentation](https://doc.periscopedata.com)

## Verified Periscope Dashboard

Some dashboards in Periscope will include a Verified Checkmark.
![Periscope Verified Checkmark](/handbook/business-ops/data-team/periscope-directory/periscope_verified_checkmark.jpg)

That means these analyses have been reviewed by the data team for query accuracy.
Dashboards without the verified checkmark are not necessarily inaccurate;
they just haven't been reviewed by the data team.


## Spaces

We have two Periscope [spaces](https://doc.periscopedata.com/article/spaces#article-title):
* GitLab
* GitLab Sandbox
* GitLab Sensitive

They connect to the data warehouse with different users- `periscope`, `periscope_staging`, and `periscope_sensitive` respectively.

Most work is present in the GitLab space, though some _extremely sensitive analyses_ will be limited to GitLab sensitive. 
Examples of this may include analyses involving contractor and employee compensation and unanonymized interviewing data.

GitLab Sandbox is a place to experiment with new reports. 
It has access to a larger set of tables than the main GitLab space, but it does not have access to the sensitive tables.
Access to this space is limited to users comfortable with SQL **and** dbt as a place to experiment visually with data that may need to be adjusted or moved to a different schema. Reports that are going to be shown to other people _should not be build in this space_.

Spaces are organized with tags. Tags should map to function (Product, Marketing, Sales, etc) and subfunction (Create, Secure, Field Marketing, EMEA). 
Tags should loosely match [issue labels](/handbook/business-ops/data-team/#issue-labeling) (no prioritization).
Tags are free. Make it as easy as possible for people to find the information they're looking for. At this time, tags cannot be deleted or renamed.

## Pushing Dashboards into Slack Automattically

Many folks will have some cadence on which they want to see dashboards;
for example, Product wants an update on opportunities lost of product reasons every week.
Where it is best that this info is piped into Slack on a regular cadence, you can take advantage of Slack's native `/remind` to print the URL.
If it does not appear that the dashboard is autorefreshing, please ping a [Periscope admin](/handbook/business-ops/#tech-stack) to update the refresh schedule.

## User Roles

There are three user roles (Access Levels) in Periscope: admin, SQL, and View Only.

The current status of Periscope licenses can be found in [the analytics project](https://gitlab.com/gitlab-data/analytics/blob/master/analyze/periscope_users.yml).

<div class="panel panel-info">
**Updating Users for Periscope**
{: .panel-heading}
<div class="panel-body">

* Inject jquery into the console (from [StackOverflow](https://stackoverflow.com/questions/45042129/how-can-i-use-jquery-in-the-firefox-scratchpad)):

```javascript
let jq = document.createElement("script");
jq.src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js";
jq.onload = function() {
  //your code here
};
document.body.appendChild(jq);
```

* On the Users page to get list of all users from the DOM, run this in the console.

```javascript
$('div.user-name').map(function(i, el) {
  return $(el).text()}
).toArray()
```

* To get all editors from the DOM, on the Groups page, after clicking on the Editors group, run this in the console:

```javascript
$('.name-text-with-globe').map(function(i, el) {
  return $(el).text()}
).toArray()
```

</div>
</div>

### Administrators

These users have the ability to provision new users, change permissions, and edit database connections. (Typical admin things)

Resource: [Onboarding Admins](https://www.youtube.com/watch?v=e-cZgf6zzlQ&feature=youtu.be)

### Editor access

The users have the ability to write SQL queries against the `analytics` schema of the `analytics` database that underlie charts and dashboards. They can also create or utilize SQL snippets to make this easier. **There are a limited number of SQL access licenses, so at this time we aim to limit teams to one per Director-led team. It will be up to the Director to decide on the best candidate on her/his team to have SQL access.**

### View only users

These users can consume all existing dashboards. They can change filters on dashboards. Finally, they can take advantage of the [Drill Down](https://doc.periscopedata.com/article/drilldowns) functionality to dig into dashboards.

### Notes for when provisioning users

Make an MR to the analytics repo updating the permissions file and link it in your provisioning message. This helps affirm who got access to what, when, and at what tier.

In the Periscope UI, navigate to the **Directory** (not the Settings. This is important since we have Spaces enabled) to add the new user using her/his first and last names and email. Then add the user to the "All Users" group  and their function group (e.g. Marketing, Product, etc.) by clicking the pencil icon on the right side of the page next to "Group". If it is an editor user, then add her/him to the "Editor" group.

Users will inherit the highest access from any group they are in. This is why all functions are by default View only.

Permissions for a group are maintained under the space "Settings" section. (This is very confusing.) To upgrade or downgrade a group, you need to do that under setting, not under the Directory.

## Dashboard Creation and Review Workflow

This section details the workflow of how to push a dashboard to "production" in Periscope. Currently, there is no functionality to have a MR-first workflow. This workflow is intended to ensure a high level of quality of all dashboards within the company. A dashboard is ready for production when the visuals, SQL, Python, and UX of the dashboard have been peer reviewed by a member of the data team and meet the standards detailed in the handbook.

1. Create a dashboard with `WIP:` as the name and add it to the `WIP` topic
1. Utilize the documentation of dbt and the warehouse to build your queries and charts
1. Once the dashboard is ready for review, create an MR [in this project](https://gitlab.com/gitlab-com/www-gitlab-com) adding the dashboard to this page and using the Periscope Dashboard Review template
1. Follow the instructions on the template
1. Assign the template to a member of the data team for review
1. Once all feedback has been given and applied, the data team member will update the text tile in the upper right corner detailing who created and reviewed the dashboard, when it was last updated, and cross-link relevant issues (See [Data Analysis Process](/handbook/business-ops/data-team/#-data-analysis-process) for more details)
1. The dashboard will be cross-linked to the above directory (with just the `Dashboard Name`, no WIP) and any original issue
1. The data team member reviewer will:
   * Rename the dashboard to remove the `WIP:` label
   * Remove the dashboard from the `WIP` topic
   * Add the Approval Badge to the dashboard
   * Merge the MR if they have permissions or assign it to someone with merge rights on the handbook

## Tips and Tricks

#### Having a Dashboard that only presents the correct fiscal quarter information

You may want a dashboard that only filters to the current fiscal quarter or the next fiscal quarter. Periscope's off-the-shelf date filters cannot accomodate for custom fiscal years. 

In your analysis, add the following: (update the `[datevalue]` with the date you're looking to have filtered)
```
LEFT JOIN analytics.date_details on current_date = date_actual
WHERE [datevalue] < last_day_of_fiscal_quarter
AND [datevalue] > first_day_of_fiscal_quarter
```

#### Working with Date Range Filters

When you have an aggregated date that you want to use as a filter on a dashboard, you have to use the aggregated period as the `date range start` and one day less than the end of the aggregation as the `date range end` value.
Your date range start value can be mapped to your date period.

![DRS](/handbook/business-ops/data-team/periscope-directory/periscope_date_range_start.png)

For the date range end, you need to create an additional column in your query to automatically calculate the end date based on the value selected in your aggregation filter. If we've been using `sfdc_opportunity_xf.close_date` as the date we care about, here is an example: `dateadd(day,-1,dateadd([aggregation],1,[sfdc_opportunity_xf.close_date:aggregation]))  as date_period_end`
Then add the mapping for the date range end.

![DRE](/handbook/business-ops/data-team/periscope-directory/periscope_date_range_end.png)
