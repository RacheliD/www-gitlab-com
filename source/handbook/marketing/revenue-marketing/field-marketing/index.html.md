---
layout: markdown_page
title: "Field Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# Field Marketing

The role of field marketing is to support marketing messages at a regional level through in-person interactions (quality vs. quantity) coupled with multi-touch activities. Field Marketing programs are focused on relationship building with customers and prospects to support land and expand opportunities as well as pulling pipeline through the funnel more quickly.

# Types of programs Field Marketing runs

## GitLab Owned Events

### GitLab Connect
GitLab Connect is a full or half day event with both customers and prospects in attendance sharing stories & lessons learned about GitLab. SAL's will be responsible for asking customers to speak and Marketing, through a combination of XDR outreach, database and ad geotargeting will drive attendance to the event. If you would like to propose a GitLab Connect in your city, please open an issue in the [Marketing - Field Marketing project](https://gitlab.com/gitlab-com/marketing/field-marketing) using the Field Event_GitLabConnect template.

To see how we handle GitLab owned events from a Marketing Programs standpoint, please head over to [their handbook page](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#event-channel-types)

### GitLab run workshops
Field Marketers will work with Product Marketing & Technical Product Marketing to put together various types of workshops depending on the needs of the region.


### 3rd party events
We will sponsor regional 3rd party events in an effort to build the GitLab brand and also to gather leads. Different type of 3rd part events include, but are not limited to:

- DevOps Days
- Agile Events
- City run technology meeting
- Customer/prospect run DevOps events on invite 
- Executive relationship building events via companies like Apex Assembly & Argyle Executive Forum

To see how we handle GitLab owned events from a Marketing Programs standpoint, please head over to [their handbook page](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#event-channel-types) 

### Field Event Goals

- Sales Acceleration
   - Engaging with existing customers
   - New growth opportunities
- Demand
   - Education
- Market Intelligence
   - Test out new messaging or positioning

### Vendors who we work with in NORAM 
Emissary.io - in an effort to help sales gain account intelligence 
Banzai - to supplement event recruiting 

## What's currently scheduled in my region?

* Note to see the full list of events, you need to be sure you are logged into your GitLab account. There are times we make issues private.
- [AMER East](https://gitlab.com/groups/gitlab-com/marketing/-/boards/915674?&label_name[]=East) - Run by @lblanchard & @jsorensen (East-Central)
- [AMER West](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933457?&label_name[]=West) - Run by @ELuehrs
- [AMER Public Sector](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933456?&label_name[]=Public%20Sector) - Run by @lblanchard
- [APAC](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933488?&label_name[]=APAC) - Run by @Phuynh
- [EMEA](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933459?&label_name[]=EMEA) - Run by @amimmo

To find out what upcoming events GitLab will be involved in please visit our [Events Page](/events/). If you have any questions or an event suggestion for us please email `fieldmarketing@gitlab.com`.

For details on how we handle events [(corporate or field, please check out this page)](Page will be forthcoming).

## How do I get a contract reviewed and approved? 
* We follow the company's procure to pay process. Please refer to the [Procure to Pay page](https://about.gitlab.com/handbook/finance/procure-to-pay/)
* Ensure your ask has been added to the [budget planning sheet](https://docs.google.com/spreadsheets/d/1DvDLnU8PcsNZtgH8a8wILV_N45JYbzuXJzGDsY0gnqU/edit?usp=sharing) with all info filled in
* Add your manager as an approver, before budget approval. Assign the issue to your manager. 

## Other pages to review for a full understanding of how Field Marketing at GitLab works
* [Marketing Program Management](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs)
* [Marketing Operations](https://about.gitlab.com/handbook/marketing/marketing-operations/)
* [Sales Development](Page will be forthcoming)

