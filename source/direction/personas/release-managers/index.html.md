---
layout: markdown_page
title: "Personas Vision - Release managers"
---

- TOC
{:toc}

## Who are release managers?
TBD

## What's next & why
- [Cross-project pipeline triggering MVC](https://gitlab.com/groups/gitlab-org/-/epics/414)

## Key Themes
- [Mature release orchestration](https://gitlab.com/groups/gitlab-org/-/epics/771)
- [Secure, compliant, and well-orchestrated deployments](/direction/release/#secure-compliant-and-well-orchestrated-deployments)
- [Zero touch delivery](/direction/release/#zero-touch-delivery)

## Stages with release management focus

There are several stages involved in developing the release manager's toolbox at GitLab. These include, but are not necessarily limited to the following:
- [Release](/direction/release)
- [Verify](/direction/verify)
